/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memrealloc.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/09 13:30:21 by agrossma          #+#    #+#             */
/*   Updated: 2018/04/09 13:48:41 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memrealloc(void *ptr, size_t size)
{
	void	*new;
	size_t	ptr_size;

	if (ptr == NULL)
		return (ft_memalloc(size));
	ptr_size = ft_memsize(ptr);
	if (size <= ptr_size)
		return (ptr);
	if (!(new = ft_memalloc(size)))
		return (NULL);
	ft_memcpy(new, ptr, ptr_size);
	ft_memdel(&ptr);
	return (new);
}
